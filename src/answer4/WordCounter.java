package answer4;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

public class WordCounter {

		private String message;
		private HashMap<String,Integer> wordCount;
		String filename = "";
		String[] word;
		FileReader fileReader = null;
		
		public WordCounter(String filenameinput){
			this.filename = filenameinput;
			wordCount = new HashMap<String,Integer>();
		}
		
		public void count() {
			try {
				FileReader fileReader = new FileReader(filename);
				BufferedReader buffer = new BufferedReader(fileReader);
				String line;
				
				for(line =  buffer.readLine(); line != null; 
					 line =  buffer.readLine()) {
					word = line.split(" ");
					for (String str : word){
						if(wordCount.containsKey(str)){
							wordCount.put(str, wordCount.get(str)+1);
						}
						else {
							wordCount.put(str, 1);
						}
					}
				}
			
			}
			catch (FileNotFoundException e) {
				System.err.println("Cannot read file "+filename);
			}
			catch (IOException e){
				 System.err.println("Error reading from file");
		 	 }
			finally {
				try {
					if (fileReader != null)
						fileReader.close();
				} catch (IOException e) {
					System.err.println("Error closing files");
				}
			}

			
		}
		
		public String getCountData() {
			String str = "";
			for (String key : this.wordCount.keySet()) {
				str += key +" = "+ this.wordCount.get(key)+"\n";
			}
			return str;
			
		}

	
}
