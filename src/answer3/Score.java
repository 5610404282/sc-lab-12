package answer3;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Score {
	public void showScore(){
		String filename = "homework.txt ";
		FileWriter fileWriter = null;
		 FileReader fileReader = null;
		try {
			int i = 0;
			double sum; 
			double avg;
			 fileReader = new FileReader(filename);
			 BufferedReader buffer = new BufferedReader(fileReader);
			 
			 fileWriter = new FileWriter("average.txt",true);
			 PrintWriter out = new PrintWriter(new BufferedWriter(fileWriter));
			 
			 String line;			 
			 System.out.println("---------- Homework Scores ----------");
			 out.println("---------- Homework Scores ----------");
			 for(line =  buffer.readLine(); line != null; 
					 line =  buffer.readLine()) {
				 String[] word = line.split(","+" ");
				 sum = Double.parseDouble(word[1]) + Double.parseDouble( word[2])+
						 Double.parseDouble(word[3])+ Double.parseDouble(word[4])+Double.parseDouble(word[5]);
				 avg = sum/5;
				 
				 
				 out.println("name : " + word[i] + "    Average score : " + avg);
				 System.out.println("name : " + word[i] + "    Average score : " + avg);
	
			 }
			 out.println("-------------------------------------");
			 out.flush();
			 System.out.println("-------------------------------------");
			 
	 	 }
	 	 catch (FileNotFoundException e){
			 System.err.println("Cannot read file "+filename);
	 	 }	 	
	 	 catch (IOException e){
			 System.err.println("Error reading from file");
	 	 }
		finally {
			try {
				if (fileReader != null){
					fileReader.close();
				}		
				fileWriter.close();
			} catch (IOException e) {
				System.err.println("Error closing files");
			}
		}
		
	}
	public void ShowExam(){
		String filename = "exam.txt ";
		FileWriter fileWriter = null;
		 FileReader fileReader = null;
		try {
			int i = 0;
			double sum; 
			double avg;
			 fileReader = new FileReader(filename);
			 BufferedReader buffer = new BufferedReader(fileReader);
			 
			 
			 fileWriter = new FileWriter("average.txt",true);
			 PrintWriter out = new PrintWriter(new BufferedWriter(fileWriter));
			 
			 String line;			 
			 System.out.println("------------ Exam Scores ------------");
			 out.println("------------ Exam Scores ------------");
			 for(line =  buffer.readLine(); line != null; 
					 line =  buffer.readLine()) {
				 String[] word = line.split(","+" ");
				 sum = Double.parseDouble(word[1]) + Double.parseDouble( word[2]);
				 avg = sum/2;
				 
				 out.println("name : " + word[i] + "    Average score : " + avg);
				 System.out.println("name : " + word[i] + "    Average score : " + avg);
	
			 }
			 out.println("-------------------------------------");
			 out.flush();
			 System.out.println("-------------------------------------");
			 
	 	 }
	 	 catch (FileNotFoundException e){
			 System.err.println("Cannot read file "+filename);
	 	 }	 	
	 	 catch (IOException e){
			 System.err.println("Error reading from file");
	 	 }
		
		finally {
			try {
				if (fileReader != null){
					fileReader.close();
				}		
				fileWriter.close();
			} catch (IOException e) {
				System.err.println("Error closing files");
			}
		}
	}
		
	
}
