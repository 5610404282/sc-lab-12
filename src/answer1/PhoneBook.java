package answer1;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class PhoneBook {
	public void display(){
		String filename = "phonebook.txt";
		FileReader fileReader = null;
	 	 try {
	 		 
	 		 int i = 0;
			 fileReader = new FileReader(filename);
			 BufferedReader buffer = new BufferedReader(fileReader);
			 String line;			 
			 System.out.println("---------- Phone Number ----------");
			 
			 for(line =  buffer.readLine(); line != null; 
					 line =  buffer.readLine()) {
				 String[] word = line.split(","+" ");
				 System.out.println("name : " + word[i] + "  number : " + word[i+1]);
	
			 }
			 System.out.println("----------------------------------");
	 	 }
	 	 catch (FileNotFoundException e){
			 System.err.println("Cannot read file "+filename);
	 	 }	 	
	 	 catch (IOException e){
			 System.err.println("Error reading from file");
	 	 }
	 	finally {
			try {
				if (fileReader != null)
						fileReader.close();
			} catch (IOException e) {
				System.err.println("Error closing files");
			}
		}

	}
	
}
